var express         = require("express"),
    app             = express(),
    mongoose        = require("mongoose"),
    flash           = require("connect-flash"),
    bodyParser      = require("body-parser"),
    methodOverride  = require("method-override"),
    Item            = require("./models/item"),
    User            = require("./models/user"),
    Comment         = require("./models/comment"),
    formidable      = require("formidable"),
    passport        = require("passport"),
    LocalStrategy   = require('passport-local').Strategy,
    passportLocalMongoose = require("passport-local-mongoose");
    
var seedDB = require('./seeds');
seedDB();
    
var commentRoutes = require("./routes/comments"),
    itemRoutes = require("./routes/items"),
    screenshotRoutes = require("./routes/screenshots"),
    indexRoutes = require("./routes/index"),
    authRoutes = require("./routes/auth");
    
    
mongoose.connect("mongodb://localhost/minecraft");

require('./config/passport')(passport); // pass passport for configuration

app.use(express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded({extended: true})); 
app.set("view engine", "ejs");
app.use(methodOverride("_method"));

app.use(require("express-session")({
    secret: "Josh is the most handsome man ever",
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

// passport.use(new LocalStrategy(User.authenticate()));
// passport.serializeUser(User.serializeUser());
// passport.deserializeUser(User.deserializeUser());

app.use(function(req, res, next){
    res.locals.currentUser = req.user;
    res.locals.error = req.flash("error");
    res.locals.success = req.flash("success");    
    next();
});

app.use(commentRoutes);
app.use(itemRoutes);
app.use(indexRoutes);
app.use(screenshotRoutes);
app.use(authRoutes);

// db.items.find({ $or: [ { "recipie.field1name": "Wooden Plank" }, { "recipie.field2name": "Wooden Plank" }, { "recipie.field3name": "Wooden Plank" }, { "recipie.field4name": "Wooden Plank" }, { "recipie.field5name": "Wooden Plank" }, { "recipie.field6name": "Wooden Plank" }, { "recipie.field7name": "Wooden Plank" }, { "recipie.field8name": "Wooden Plank" }, { "recipie.field9name": "Wooden Plank" } ] })

app.listen(8080);
console.log('Server has started.');