var express = require("express");
var router = express.Router();
var Item = require("../models/item");
var User = require("../models/user");
var passport = require("passport");
var middleware = require("../middleware");


// ==============
// INDEX
// ==============

router.get("/", function(req, res) {
    res.render("landing");
});

router.get("/adminpanel", function(req, res) {
    User.find({}, function(err, users) {
        if (err) {
            console.log(err);
        } else {
            res.render("adminpanel", {users: users});
        }
    })
})



// ================
// SEARCH BAR ROUTES
// ================

// SEARCH BAR SUGGEST
router.get('/search_member', function(req, res) {
   var regex = new RegExp(req.query["term"], 'i');
   var query = Item.find({name: regex}, { 'name': 1 }).sort({"_id":-1}).limit(20);
        
      // Execute query in a callback and return users list
  query.exec(function(err, users) {
      if (!err) {
         // Method to construct the json result set
         var result = middleware.buildResultSet(users); 
         res.send(result, {
            'Content-Type': 'application/json'
         }, 200);
      } else {
          console.log("here.");
         res.send(JSON.stringify(err), {
            'Content-Type': 'application/json'
         }, 404);
      }
   });
});

// SEARCH BAR SEND
router.get("/search", function(req, res) {
    var name = req.query.name;
    Item.find({name: name}, function(err, foundItem) {
        if(err) {
            console.log(err);
        } else {
                if (undefined != foundItem[0]) {
                    res.redirect("/item/" + foundItem[0]._id);
                } else {
                   console.log(name + " was not found.");
                }
            }
    });
});

// Get recipie
router.get("/getrecipe", function(req, res) {
    var itemname = req.query.search;
    Item.find({name: itemname}, function(err, foundItem) {
        if(err) {
            console.log("stopped here.");
            console.log(err);
        } else {
            if(foundItem[0] == undefined) {
                console.log(itemname + "not found");
            } else {
              var i = [foundItem[0].image, foundItem[0]._id, itemname];
              console.log("array to be added:");
              console.log(i);
              res.send(i);
            }
        }
})});


module.exports = router;