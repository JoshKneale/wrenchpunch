var express = require("express");
var router = express.Router();
var Item = require("../models/item");
var middleware = require("../middleware");


// INDEX
router.get("/index", function(req, res) {
    Item.find({}, function(err, allItems){
       if(err){
           console.log(err);
       } else {
          res.render("index",{items:allItems});
       }
    });
});

// SHOW
router.get("/item/:_id", function(req, res) {
    Item.findById(req.params._id).populate("comments screenshots").exec(function(err, foundItem){
        if(err){
            console.log(err);
        } else {
            if(foundItem == null) {
                req.flash("error", "This item is missing, let Kaap know on the site discord!");
                res.redirect("/index");
                console.log("Item missing and causing a broken link: " + req.params._id)
            } else {
                // Build query
                var query = {$or : [ { "recipie.field1name": foundItem.name }, { "recipie.field2name": foundItem.name }, { "recipie.field3name": foundItem.name }, { "recipie.field4name": foundItem.name }, { "recipie.field5name": foundItem.name }, { "recipie.field6name": foundItem.name }, { "recipie.field7name": foundItem.name }, { "recipie.field8name": foundItem.name }, { "recipie.field9name": foundItem.name } ]}
                // Query DB
                Item.find(query, function(err, usedin) {
                    if (err) {
                        console.log(err); 
                    } else {
                        // Respond with results
                        res.render("show", {item: foundItem, usedIn: usedin});
                    }
                })
            }
        }
    });
}); 

// NEW
router.get("/new", middleware.isLoggedin, middleware.isAdmin, function(req,res) {
    res.render("new");
});

// CREATE
router.post("/new" , middleware.isLoggedin, function(req, res) {

    // Add recipie object to item
    var item = req.body.newItem;
    item.recipie = req.body.recipie;

    // Create a new item and save to DB
    Item.create(item, function(err, newlyCreated){
        if(err){
            console.log(err);
        } else {
            //redirect to new item page
            req.flash("success", "Item created!");
            res.redirect("/item/" + newlyCreated._id);
        }
    });
});

// EDIT
router.get("/item/:id/edit", middleware.isLoggedin, function(req, res){
    Item.findById(req.params.id, function(err, foundItem){
        res.render("edit", {item: foundItem});
    });
});

// UPDATE
router.put("/item/:_id", middleware.isLoggedin,function(req, res){

    var item = req.body.editedItem;
    item.recipie = req.body.recipie;

    // find and update the correct campground
    Item.findByIdAndUpdate(req.params._id, item, function(err, updatedItem){
       if(err){
           res.redirect("/index");
       } else {
           //redirect somewhere(show page)
           console.log(updatedItem.name + " updated.");
           res.redirect("/item/" + req.params._id);
       }
    });
});

// DELETE
router.delete("/item/:_id", middleware.isLoggedin, function(req, res){
   Item.findByIdAndRemove(req.params._id, function(err){
      if(err){
          res.send("something went wrong");
          console.log(err);
      } else {
          req.flash("success", "Item deleted.");
          res.redirect("/index");
      }
   });
});

// Tooltip AJAX
router.get('/tooltip', function(req, res) {
    Item.findOne({name: req.query.name}, function(err, item) {
        if(err) {
            console.log(err);
            res.send(400);
        } else {
            if (item == null) {
                var data = "Item not found.";
                res.send(data, {
                'Content-Type': 'application/json'
                }, 200);
            } else {
                var data = "<a href='/item/"+item._id+"'><img class='tooltipimage' src='/itemimages/"+ item.image + "' /> <strong>"+item.name+"</strong> - "+item.mod+"</a>";
                res.send(data, {
                'Content-Type': 'application/json'
                }, 200);
            }
        }
    })
})

module.exports = router;