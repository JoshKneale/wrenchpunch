var express = require("express");
var router = express.Router();
var middleware = require("../middleware");
var formidable = require("formidable");
var fs = require("fs");
var path = require('path');
var Item = require("../models/item");
var Screenshot = require("../models/screenshot");
var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: '',
        pass: ''
    }
})

router.post('/uploadscreenshot/:_id', middleware.isLoggedin, function(req, res){

  var userID = req.user._id;
  var userName = req.user.local.username;

  // create an incoming form object
  var form = new formidable.IncomingForm();
  
  // specify that we want to allow the user to upload multiple files in a single request
  form.multiples = false;

  // store all uploads in the /uploads directory
  form.uploadDir = path.join('./public/screenshots');

  // every time a file has been uploaded successfully,
  // rename it to it's original name
  form.on('file', function(field, file) {
    fs.rename(file.path, path.join(form.uploadDir, req.params._id + "-" + file.name));
    
    var data = ({filename: file.name});
    
     Item.findById(req.params._id, function(err, item) {
        if(err) {
            console.log(err);
            res.redirect("/index");
        } else {
            Screenshot.create(data, function(err, screenshot) {
                if(err) {
                    console.log(err);
                } else {
                    // add username and id to comment
                    screenshot.author.id = userID;
                    screenshot.author.username = userName;
                    screenshot.created = Date();
                    // save screenshot
                    screenshot.save();
                    item.screenshots.push(screenshot);
                    item.save();

                    var mailOptions = {
                        from: 'Wrenchpunch <kaap@wrenchpunch.com>', // sender address
                        to: 'kaap@wrenchpunch.com',
                        subject: 'New Screenshot on ' + item.name, // Subject line
                        html:   '<p>New screenshot posted on ' + item.name + '.</p>'+
                                '<p><b>Content:</b></p>'+
                                '<p><a href="https://wrenchpunch.com/item/' + item._id + '">View Item Page</a></p>'
                    };
                    console.log(mailOptions);

                    // send mail with defined transport object
                    transporter.sendMail(mailOptions, (error, info) => {
                        if (error) {
                            return console.log(error);
                        }
                        console.log('Email sent to' + email, info.messageId, info.response);
                    });
                }
                });
            }
         });
  });
    

  // log any errors that occur
  form.on('error', function(err) {
    console.log('An error has occured: \n' + err);
  });
  

  // once all the files have been uploaded, send a response to the client
  form.on('end', function() {
    res.end('success');
  });

  // parse the incoming request containing the form data
  form.parse(req);

});

router.delete("/item/:_id/screenshots/:screenshot_id", middleware.checkScreenshotOwnership, function(req, res) {
    
    // Find Item
    Screenshot.findById(req.params.screenshot_id, function(err, screenshot) {
        if(err) {
            res.redirect("back");
        } else {
            // Check if file exists
            var fileLocation = "./public/screenshots/" + req.params._id +"-"+screenshot.filename;
            fs.stat(fileLocation, function(err, fileStat) {
                if (err) {
                    if (err.code == 'ENOENT') {
                        console.log('Does not exist.');
                    }
                } else {
                    if (fileStat.isFile()) {
                        console.log('File found.');
                        
                        // Delete the file
                        fs.unlink(fileLocation, function (err) {
                          if (err) throw err;
                          console.log('Deletion successful.');
                        });
                        
                    } else if (fileStat.isDirectory()) {
                        console.log('Directory found.');
                    }
                }
            });
            
        }
    });
    
    // Delete the Screenshot
    Screenshot.findByIdAndRemove(req.params.screenshot_id, function(err) {
        if(err) {
            res.redirect("back");
        } else {
            res.redirect("/item/" + req.params._id);
        }
    });
});


module.exports = router;