var express     = require('express'),
    router      = express.Router(),
    User        = require('../models/user'),
    Comment     = require('../models/comment'),
    passport    = require('passport'),
    middleware = require("../middleware"),
    async = require('async'),
    crypto = require('crypto');

var nodemailer = require('nodemailer');

    // create reusable transporter object using the default SMTP transport
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: '',
            pass: ''
        }
    });

// ===========
// Routes
// ===========

// Login View
router.get("/login" , function(req,res) {
    res.render("auth/login");
});

// Log user in
router.post('/login', function(req, res, next) {
  passport.authenticate('local-login', function(err, user, info) {
    if (err) { return next(err); }
    // Redirect if it fails
    if (!user) { return res.redirect('/login'); }
    req.logIn(user, function(err) {
      if (err) { return next(err); }
      if (!user.local.verified) { return res.render('auth/emailconfirm', {user: user}); }
      // Redirect if it succeeds
      var redirectlink = req.session.returnTo;
      req.session.returnTo = null;
      res.redirect(redirectlink || '/');
    });
  })(req, res, next);
});

// Register View
router.get("/register", function(req, res) {
    res.render("auth/register");
});

// Register a User
router.post('/register', function(req, res, next) {
  passport.authenticate('local-signup', function(err, user, info) {
    if (err) { return next(err); }
    if (!user) { return res.redirect('/register'); }
    req.logIn(user, function(err) {
      if (err) { return next(err); }
      return res.render('auth/emailconfirm', {user: user});
    });
  })(req, res, next);
});

// Resend verification email
router.post('/resend', function(req, res) {
    User.findById(req.user._id, function(err, user) {
        var permalink = user.local.permalink;
        var verification_token = user.local.verify_token;
        var email = user.local.email;
        // setup email data with unicode symbols
        var confirmlink = "https://wrenchpunch.com/verify/" +permalink+ "/" + verification_token;
        var mailOptions = {
            from: 'Wrenchpunch <kaap@wrenchpunch.com>', // sender address
            to: email, // list of receivers
            subject: 'Confirm your wrenchpunch.com account', // Subject line
            text: 'Hello! Click this link to confirm your account with wrenchpunch.com: ' + confirmlink, // plain text body
            html:   '<p><b>Hello from wrenchpunch.com!</b></p>'+
                    '<p>Click the link below to confirm your account:</p>'+
                    '<p><a href="'+ confirmlink +'">Confirm account</a></p>' +
                    '<p>Or alternatively copy this link into your internet browser:</p>' +
                    '<p>'+ confirmlink +'</p>' +
                    '<br><p><i>Having touble? Get in contact with us on our site discord: <a>https://discord.gg/uZYDbFT</a><i></p>'// html body
        };
        console.log(mailOptions);

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Email sent to' + email, info.messageId, info.response);
            res.sendStatus(200);
        });
    })
});

// Authenticate user with email link
router.get('/verify/:permalink/:token', function (req, res) {
        var permalink = req.params.permalink;
        console.log(permalink);
        var token = req.params.token;
        console.log(token);

        User.findOne({'local.permalink': permalink}, function (err, user) {
            if (user.local.verify_token == token) {
                console.log('that token is correct! Verify the user');

                User.findOneAndUpdate({'local.permalink': permalink}, {'local.verified': true}, function (err, resp) {
                    console.log('The user has been verified!');
                });
                req.flash("success", "Account verified! Please login below");
                res.redirect('/login');
            } else {
                console.log('The token is wrong! Reject the user. token should be: ' + user.local.verify_token);
            }
        });
    });

// Log out the user
router.get("/logout", function(req,res) {
    req.logout();
    res.redirect("/");
});


// Show the profile page
router.get("/profile", middleware.isLoggedin, function(req, res) {
    Comment.find({'author.id' : req.user._id }).limit(5).exec(function(err, foundComments){
      if(err) {console.log(err)}
      else {
        res.render("profile", { user: req.user, comments: foundComments })
      }
    })
});

// Show forgot password page
router.get("/forgot", function(req, res) {
    res.render("auth/forgot");
})

// Send reset password email
router.post('/forgot', function(req, res, next) {
  async.waterfall([
    function(done) {
      crypto.randomBytes(20, function(err, buf) {
        var token = buf.toString('hex');
        done(err, token);
      });
    },
    function(token, done) {
      User.findOne({ 'local.email': req.body.email }, function(err, user) {
        if (!user) {
          req.flash('error', 'No account with that email address exists.');
          return res.redirect('/forgot');
        }

        user.local.resetPasswordToken = token;
        user.local.resetPasswordExpires = Date.now() + 3600000; // 1 hour

        user.save(function(err) {
          done(err, token, user);
        });
      });
    },
    function(token, user, done) {
      var mailOptions = {
        to: user.local.email,
        from: 'Wrenchpunch noreply@wrenchpunch.com',
        subject: 'Wrenchpunch.com account password reset',
        text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
          'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
          'https://' + req.headers.host + '/reset/' + token + '\n\n' +
          'If you did not request this, please ignore this email and your password will remain unchanged.\n'
      };
      transporter.sendMail(mailOptions, function(err) {
        req.flash('success', 'An e-mail has been sent to ' + user.local.email + ' with further instructions.');
        done(err, 'done');
      });
    }
  ], function(err) {
    if (err) return next(err);
    res.redirect('/forgot');
  });
});

// reset password incoming link - renders reset page
router.get('/reset/:token', function(req, res) {
  User.findOne({ 'local.resetPasswordToken': req.params.token, 'local.resetPasswordExpires': { $gt: Date.now() } }, function(err, user) {
    if (!user) {
      req.flash('error', 'Password reset token is invalid or has expired.');
      return res.redirect('/forgot');
    }
    res.render('auth/reset', {user: req.user, token: req.params.token});
  });
});

// reset user password and forward to home page
router.post('/reset/:token', function(req, res) {
  async.waterfall([
    function(done) {
      User.findOne({ 'local.resetPasswordToken': req.params.token, 'local.resetPasswordExpires': { $gt: Date.now() } }, function(err, user) {
        if (!user) {
          req.flash('error', 'Password reset token is invalid or has expired.');
          return res.redirect('back');
        }

        user.local.password = req.body.password;
        user.local.resetPasswordToken = undefined;
        user.local.resetPasswordExpires = undefined;

        user.save(function(err) {
          req.logIn(user, function(err) {
            done(err, user);
          });
        });
      });
    },
    function(user, done) {
      var mailOptions = {
        to: user.local.email,
        from: 'Wrenchpunch noreply@wrenchpunch.com',
        subject: 'Your password has been changed',
        text: 'Hello,\n\n' +
          'This is a confirmation that the password for your account ' + user.local.email + ' has just been changed.\n'
      };
      transporter.sendMail(mailOptions, function(err) {
        req.flash('success', 'Success! Your password has been changed.');
        done(err);
      });
    }
  ], function(err) {
    res.redirect('/');
  });
});

// //Change username
// router.post("/profile", function(req, res) {
//   User.findOne({'local.username' : req.body.username}, function(err, foundName) {
//     if (err) {console.log(err)}
//     else {
//       if (foundName) {
//         req.flash("error", "That username already exists");
//         res.render('profileedit');
//       } else {
//         User.findById(req.user._id, function(err, foundUser) {
//           if(err) {console.log(err)}
//           else {
//             foundUser.local.username = req.body.username;
//             foundUser.save();
//             req.flash('success', 'Username updated');
//             res.render('profile');
//           }
//         })
//       }
//     }
//   })
// })
module.exports = router;

