var express = require("express");
var router = express.Router();
var Item = require("../models/item");
var Comment = require("../models/comment");
var middleware = require("../middleware");
var Filter = require('bad-words'),
  filter = new Filter();
var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: '',
        pass: ''
    }
});

// Create Comment
router.post("/item/:_id/comments", middleware.isLoggedin, function(req, res) {
    //Look up item using ID
    Item.findById(req.params._id, function(err, item) {
        if(err) {
            console.log(err);
            res.redirect("/index");
        } else {
            Comment.create(req.body.comment, function(err, comment) {
                if(err) {
                    console.log(err);
                } else {
                    // add username and id to comment
                    comment.author.id = req.user._id;
                    comment.author.username = req.user.local.username;
                    comment.created = Date();
                    comment.location = item.name;
                    comment.locationID = item._id;
                    //Filter comment text
                    comment.text = filter.clean(comment.text);
                    // save comment
                    comment.save();
                    
                    item.comments.push(comment);
                    item.save();

                    var mailOptions = {
                        from: 'Wrenchpunch <kaap@wrenchpunch.com>', // sender address
                        to: 'kaap@wrenchpunch.com',
                        subject: 'New Comment on ' + item.name, // Subject line
                        text: 'Comment content:' + comment.text, // plain text body
                        html:   '<p>New comment posted on <a href="http://wrenchpunch.com/item.'+ item._id +'">' + item.name + '</a></p>'+
                                '<p><b>Content:</b></p>'+
                                '<p>'+ comment.text +'</p>'
                    };
                    console.log(mailOptions);

                    // send mail with defined transport object
                    transporter.sendMail(mailOptions, (error, info) => {
                        if (error) {
                            return console.log(error);
                        }
                        console.log('Email sent to' + email, info.messageId, info.response);
                    });

                    res.redirect("/item/" + item._id);
                }
            });
        }
    });
});

// Edit and Update Comment
router.put("/item/:_id/comments/:comment_id", middleware.checkCommentOwnership, function(req, res) {
    var comment = {text: filter.clean(req.body.comment.text)};
    Comment.findByIdAndUpdate(req.params.comment_id, comment, function(err, updatedComment) {
        if(err) {
            res.redirect("back");
        } else {
            req.flash("success", "Comment updated!");
            res.redirect("/item/" + req.params._id);
        }
    });
});

// Delete Comment
router.delete("/item/:_id/comments/:comment_id", middleware.checkCommentOwnership, function(req, res) {
    Comment.findByIdAndRemove(req.params.comment_id, function(err) {
        if(err) {
            res.redirect("back");
        } else {
            req.flash("success", "Comment deleted!");
            res.redirect("/item/" + req.params._id);
        }
    });
});


module.exports = router;