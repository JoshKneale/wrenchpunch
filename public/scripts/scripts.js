// Toggle recipie input
$( "#recipetoggle" ).click(function() {
 $( "#recipefields" ).toggle(300);
});

// Search bar autcomplete
$(function () {
  $(".search-query").autocomplete({
      source: function (request, response) {
         $.ajax({
            url: "/search_member",
            type: "GET",
            data: request,  // request is the value of search input
            success: function (data) {
              // Map response values to field label and value
               response($.map(data, function (el) {
                  return {
                     label: el.name,
                     value: el._id
                  };
                  }));
               }
            });
         },
         
         // The minimum number of characters a user must type before a search is performed.
         minLength: 2, 
         
         // set an onFocus event to show the result on input field when result is focused
         focus: function (event, ui) { 
            this.value = ui.item.label; 
            // Prevent other event from not being execute
            event.preventDefault();
         },
         select: function (event, ui) {
            // Prevent value from being put in the input:
            this.value = ui.item.label;
            // Set the id to the next input hidden field
            $(this).next("input").val(ui.item.value); 
            // Prevent other event from not being execute            
            event.preventDefault();
            // optional: submit the form after field has been filled up
            $('#quicksearch').submit();
         }
  });
});

// Search bar execute
$(".search-query").keypress(function(event) {
   if(event.which === 13){
      var itemname = { search: $(this).val() };
      var num = $(this).attr('id');
      console.log("id of current:");
      console.log(num);
      console.log("before ajax request:");
      console.log(itemname);
      $.get("/getrecipe", itemname, function(data) {
         console.log("returned ajax request:");
         console.log(data);
         $("#div"+num+"").replaceWith('<p>Stored:</p><input class="form-control" type="text" name="recipie[field'+ num + 'name]" value="'+ data[2] +'" readonly><input class="form-control" type="text" name="recipie[field'+ num + 'image]" value="'+ data[0] +'" readonly><input class="form-control" type="text" name="recipie[field'+ num +'link]" value="'+ data[1] +'" readonly>');
      });
}});

// Hides broken image icons
document.addEventListener("DOMContentLoaded", function(event) {
   document.querySelectorAll('img').forEach(function(img){
  	img.onerror = function(){this.style.display='none';};
   });
});

// Tooltips
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

// Tabs
$('#myTabs a').click(function (e) {
 e.preventDefault()
 $(this).tab('show')
})

//Resend Verification Email
$("#resend").click(function(e){
          e.preventDefault();
        $.ajax({type: "POST",
                url: "/resend",
                data: {},
                complete:function(){
          $("#resend-confirm").toggle();
        }});
      });

//Tooltipster
$(document).ready(function() {
    $('.tooltipster').tooltipster({
      theme: 'tooltipster-shadow',
      content: 'Loading...',
    // 'instance' is basically the tooltip. More details in the "Object-oriented Tooltipster" section.
    functionBefore: function(instance, helper) {
        var $origin = $(helper.origin);
        
        // we set a variable so the data is only loaded once via Ajax, not every time the tooltip opens
        if ($origin.data('loaded') !== true) {
            var item = { name: $(helper.origin).text() }
            $.get('/tooltip', item, function(data) {

                // call the 'content' method to update the content of our tooltip with the returned data.
                // note: this content update will trigger an update animation (see the updateAnimation option)
                instance.content($.parseHTML(data));

                // to remember that the data has been loaded
                $origin.data('loaded', true);
            });
        }
    },
    interactive: true,
    trackTooltip: true
    });
});

// Toggle recipe input
$( "#clearrecipes" ).click(function(e) {
    e.preventDefault();
 $( "#div1, #div2, #div3, #div4, #div5, #div6, #div7, #div8, #div9" ).empty();
});

// check passwords match
function checkPass()
{
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('pass1');
    var pass2 = document.getElementById('pass2');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field 
    //and the confirmation field
    if(pass1.value == pass2.value){
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords Match!"
        $( "#submitbutton" ).show();
    }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
         $( "#submitbutton" ).hide();
    }
}  