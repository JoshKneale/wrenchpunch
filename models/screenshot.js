var mongoose = require("mongoose");

var screenshotSchema = mongoose.Schema({
    filename: String,
    created: Date,
    author: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User"
        },
        username: String
    }
});

module.exports = mongoose.model("Screenshot", screenshotSchema);