var mongoose = require("mongoose");

var ItemSchema = new mongoose.Schema({
    name: String,
    summary: String,
    image: String,
    obtain: String,
    recipereq: Array,
    recipie: {
        field1name: {type: String, default: null },
        field1link: {type: String, default: null },
        field1image: {type: String, default: null },
        field2name: {type: String, default: null },
        field2link: {type: String, default: null },
        field2image: {type: String, default: null },
        field3name: {type: String, default: null },
        field3link: {type: String, default: null },
        field3image: {type: String, default: null },
        field4name: {type: String, default: null },
        field4link: {type: String, default: null },
        field4image: {type: String, default: null },
        field5name: {type: String, default: null },
        field5link: {type: String, default: null },
        field5image: {type: String, default: null },
        field6name: {type: String, default: null },
        field6link: {type: String, default: null },
        field6image: {type: String, default: null },
        field7name: {type: String, default: null },
        field7link: {type: String, default: null },
        field7image: {type: String, default: null },
        field8name: {type: String, default: null },
        field8link: {type: String, default: null },
        field8image: {type: String, default: null },
        field9name: {type: String, default: null },
        field9link: {type: String, default: null },
        field9image: {type: String, default: null },
    },
    recipeoutput: Number,
    mcid: String,
    idname: String,
    stackable: Number,
    blastres: String,
    toolreq: String,
    flammable: String,
    mod: String,
    comments: [
      {
         type: mongoose.Schema.Types.ObjectId,
         ref: "Comment"
      }
   ],
   screenshots: [
       {
       type: mongoose.Schema.Types.ObjectId,
       ref: "Screenshot"
      }
   ]
});

module.exports = mongoose.model("Item", ItemSchema);