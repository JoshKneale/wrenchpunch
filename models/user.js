var mongoose = require("mongoose");
var bcrypt   = require('bcrypt-nodejs');
var randomstring     = require('randomstring');

var userSchema = new mongoose.Schema({
    local            : {
        email        : String,
        password     : String,
        permalink    : String,
        verify_token : String,
        verified     : Boolean,
        username     : { type: String, default: "user" + randomstring.generate({length: 10})},
        role        : String,
        created     : Date,
        resetPasswordToken : String,
        resetPasswordExpires : Date
    }
});

// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);