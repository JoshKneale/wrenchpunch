var mongoose = require("mongoose");

var commentSchema = mongoose.Schema({
    text: String,
    created: Date,
    location: String,
    locationID: String,
    author: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User"
        },
        username: String
    }
});

module.exports = mongoose.model("Comment", commentSchema);