var Comment = require("../models/comment");
var Screenshot = require("../models/screenshot");


var middlewareObj = {};

middlewareObj.checkCommentOwnership = function(req, res, next) {
    if(req.isAuthenticated()) {
        Comment.findById(req.params.comment_id, function(err, foundComment) {
            if(err) {
                res.redirect("back");
            } else {
                if(foundComment.author.id.equals(req.user._id) || req.user.local.role == "admin") {
                    next();
                } else {
                    res.redirect("back");
                }
            }
        });
    } else {
        req.flash("error", "You are not the owner of that comment.");
        res.redirect("back");
    }
};

middlewareObj.isLoggedin = function(req, res, next) {
    if(req.isAuthenticated()){
        return next();
    }
    req.session.returnTo = req.path;
    req.flash("error", "You must be logged in to do that.");
    res.redirect("/login");
};

middlewareObj.isAdmin = function(req, res, next) {
    if(req.isAuthenticated()){
        if(req.user.local.role == "admin") {
            return next();
        }
        req.flash("error", "You do not have permission to do that");
        res.redirect("/");
    }
    req.flash("error", "You do not have permission to do that");
    res.redirect("/");
}

middlewareObj.buildResultSet = function(docs) {
    var result = [];
    for(var object in docs){
      result.push(docs[object]);
    }
    return result;
   };

middlewareObj.checkScreenshotOwnership = function(req, res, next) {
    if(req.isAuthenticated()) {
        Screenshot.findById(req.params.screenshot_id, function(err, foundScreenshot) {
            if(err) {
                res.redirect("back");
            } else {
                if(foundScreenshot.author.id.equals(req.user._id) || req.user.local.role == "admin") {
                    next();
                } else {
                    res.redirect("back");
                }
            }
        });
    } else {
        req.flash("error", "You are not the owner of that screenshot!");
        res.redirect("back");
    }
};

middlewareObj.requireRole = function(role) {
    return function(req, res, next) {
        if(req.session.user && req.session.user.role === role)
            next();
        else
            res.send(403);
    }
}

module.exports = middlewareObj;