// load all the things we need
var LocalStrategy    = require('passport-local').Strategy;
var randomstring     = require('randomstring');

// load up the user model
var User       = require('../models/user');

// Nodemailer
var nodemailer = require('nodemailer');

    // create reusable transporter object using the default SMTP transport
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: '',
            pass: ''
        }
    });

module.exports = function(passport) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    },
    function(req, email, password, done) {

        // asynchronous
        process.nextTick(function() {
            User.findOne({ 'local.email' :  email }, function(err, user) {
                // if there are any errors, return the error
                if (err)
                    return done(err);

                // if no user is found, return the message
                if (!user)
                    return done(null, false, req.flash('error', 'No user found.'));

                if (!user.validPassword(password))
                    return done(null, false, req.flash('error', 'Oops! Wrong password.'));
                    
                // all is well, return user
                else
                    return done(null, user);
            });
        });

    }));

    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    passport.use('local-signup', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true // allows us to pass back the entire request to the callback
    },

    function (req, email, password, done) {
        // asynchronous
        // User.findOne wont fire unless data is sent back
        process.nextTick(function () {
            // find a user whose email is the same as the forms email
            // we are checking to see if the user trying to login already exists
            User.findOne({'local.email': email}, function (err, user) {
                // if there are any errors, return the error
                if (err) {
                    return done(err);
                }

                // check to see if theres already a user with that email
                if (user) {
                    console.log('that email exists');
                    return done(null, false, req.flash('error', email + ' is already in use. '));

                } else {
                    User.findOne({'local.username': req.body.username}, function (err, user) {
                        if (user) {
                            console.log('That username exists');
                            return done(null, false, req.flash('error', 'That username is already taken.'));
                        }

                        else {
                            // create the user
                            var newUser = new User();

                            var permalink = req.body.username.toLowerCase().replace(' ', '').replace(/[^\w\s]/gi, '').trim();

                            var verification_token = randomstring.generate({
                                length: 64
                            });

                            newUser.local.username = req.body.username;

                            newUser.local.created = Date();

                            newUser.local.role = "user";

                            newUser.local.email = email;

                            newUser.local.password = newUser.generateHash(password);

                            newUser.local.permalink = permalink;

                            //Verified will get turned to true when they verify email address
                            newUser.local.verified = false;
                            newUser.local.verify_token = verification_token;

                            try {
                                newUser.save(function (err) {
                                    if (err) {

                                        throw err;
                                    } else {
                                        // setup email data with unicode symbols
                                        var confirmlink = "https://wrenchpunch.com/verify/" +permalink+ "/" + verification_token;
                                        var mailOptions = {
                                            from: 'Wrenchpunch <kaap@wrenchpunch.com>', // sender address
                                            to: email, // list of receivers
                                            subject: 'Confirm your wrenchpunch.com account', // Subject line
                                            text: 'Hello! Click this link to confirm your account with wrenchpunch.com: ' + confirmlink, // plain text body
                                            html:   '<p><b>Hello from wrenchpunch.com!</b></p>'+
                                                    '<p>Click the link below to confirm your account:</p>'+
                                                    '<p><a href="'+ confirmlink +'">Confirm account</a></p>' +
                                                    '<p>Or alternatively copy this link into your internet browser:</p>' +
                                                    '<p>'+ confirmlink +'</p>' +
                                                    '<br><p><i>Having touble? Get in contact with us on our site discord: <a>https://discord.gg/uZYDbFT</a><i></p>'// html body
                                        };
                                        console.log(mailOptions);

                                        // send mail with defined transport object
                                        transporter.sendMail(mailOptions, (error, info) => {
                                            if (error) {
                                                return console.log(error);
                                            }
                                            console.log('Email sent to' + email, info.messageId, info.response);
                                        });
                                        return done(null, newUser);
                                    }
                                });
                            } catch (err) {

                            }
                        }
                    });
                }
            });
        });
    }));
};