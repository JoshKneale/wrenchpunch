var mongoose = require('mongoose'),
    User        = require('./models/user'),
    Comment     = require('./models/comment'),
    Screenshot  = require('./models/screenshot');

User.findOne({_id : '58e904fe4554810a7482fa1f' }, function (err, user) {
    if(user){
    user.local.role = "admin";
    user.save();
    console.log("admin updated");
    }
});

var data = [
    {
        name: 'Wood',
        summary: 'Wood is the best, I love wood.',
        image: 'Oak_Wood.png',
        obtain: 'You get Wood from chopping trees with an Axe.',
        recipereq: ['No'],
        recipie: {},
        recipeoutput: 0,
        mcid: '12345',
        idname: 'wood',
        stackable: 64,
        blastres: 12.5,
        toolreq: 'Axe',
        flammable: 'Yes',
        mod: 'Vanilla Minecraft',
        comments: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Comment"
        }
    ],
    screenshots: [
        {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Screenshot"
        }
    ]
        }
    ]

    function seedDB() {
        User.remove({}, function(err) {
            console.log("Users Removed.");
        });

        Comment.remove({}, function(err) {
            console.log("Comments Removed.");
        });

        Screenshot.remove({}, function(err) {
            console.log("Screenshots Removed.");
        });
}

module.exports = seedDB;